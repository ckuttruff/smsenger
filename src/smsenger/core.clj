(ns smsenger.core
  (:use [compojure.core  :only [POST defroutes]]
        [ring.util.codec :only [url-encode]])
  (:require [clojure.data.json  :as json]
            [compojure.route    :as route]
            [compojure.handler  :as handler]
            [ring.adapter.jetty :as jetty]
            [smsenger.caching   :as cache]
            [smsenger.http      :as http]))

(def default-num "+19513142670")

(defn search-req [term]
  (let [resp (http/wiki-search term)
        json-str (json/read-str (:body resp))
        abstract (get json-str "Abstract")
        res (if (clojure.string/blank? abstract)
              "No results" abstract)]
    (cache/store term res)
    res))

(defn format-res [res]
  (let [max-char (min (count res) 160)]
    (subs res 0 max-char)))

(defroutes app-routes
  (POST "/" {{num :From body :Body :as params} :params}
        (let [res (or (cache/fetch body)
                      (search-req body))]
          (http/twilio-send {:to (or num default-num)
                             :body res})
          {:status 200
           :headers {"Content-Type" "application/json; charset=utf-8"}
           :body (format-res res)}))
  (route/files "/")
  (route/not-found "Page not found"))

(def app
  (handler/site app-routes))

(defn -main []
  (jetty/run-jetty #'app-routes {:port 8080}))
