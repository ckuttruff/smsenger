(ns smsenger.http
  (:use [ring.util.codec :only [url-encode form-encode]])
  (:require [clj-http.client :as http]
            [clojure.data.json :as json]))

;; ---------------------------------------------------------------------------
;; Toplevel config (url's auth data, etc)
(def ddg "https://api.duckduckgo.com")
;; Twilio urls / etc
(def twilio-api-date "2010-04-01")
(def twilio-acct-num  (System/getenv "TWILIO_ACCT_SID"))
(def twilio-phone-num (System/getenv "TWILIO_PHONE_NUM"))
(def twilio-token     (System/getenv "TWILIO_TOKEN"))
(def twilio-base-url "https://api.twilio.com")
(def twilio-url
  (format "%s/%s/Accounts/%s/Messages.json"
          twilio-base-url twilio-api-date twilio-acct-num))

(defn wiki-search [query-str]
  (let [query (url-encode "wikipedia" query-str)
        url   (format "%s/?q=%s&%s" ddg query "format=json")]
    (http/get url)))


(defn twilio-send [{to :to body :body}]
  (let [params {:To   to
                :From twilio-phone-num
                :Body body}]
    (try
      (http/request {:method :post
                     :url twilio-url
                     :as :json
                     :form-params params
                     :basic-auth [twilio-acct-num twilio-token]})
      (catch Exception e
        (println e)))))
