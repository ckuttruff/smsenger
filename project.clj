(defproject smsenger "0.1.0"
  :description "text search with clojure/compojure/twilio"
  :url ""
  :license {}
  :dependencies [[org.clojure/clojure "1.5.1"]
                 [org.clojure/data.json "0.2.4"]
                 [ring/ring-jetty-adapter "1.2.1"]
                 [clj-http "0.9.1"]
                 [com.taoensso/carmine "2.6.2"]
                 [compojure "1.1.6"]
                 [hiccup "1.0.5"]]

  :plugins [[lein-ring "0.8.10"]
            [cider/cider-nrepl "0.7.0-SNAPSHOT"]]


  :main smsenger.core
  :ring {:handler smsenger.core/app
         :reload-paths ["src" "migrations"]})
